from bs4 import BeautifulSoup
with open('3D Load Calculator - PIER2PIER.COM.html') as f:
    raw = f.read()
soup = BeautifulSoup(raw, 'html.parser')
form_inputs = soup.find_all(class_='form-control-sm')
data = {i.get('name'): i.get('value') for i in form_inputs}
from collections import defaultdict
correlated_data = defaultdict(dict)
for k, v in data.items():
    if k != None:
        parts = k.split('_')
        field, idx = '_'.join(parts[:-1]), parts[-1]
        correlated_data[idx][field] = v
correlated_data
from collections import namedtuple
Item = namedtuple('Item', ['n', 'l', 'w', 'h', 'q'])
parsed_items = []
for i in correlated_data.values():
    if i.get('height') == None:
        continue
    if i.get('width') == None:
        i['width'] = i['height']
    vals = [i['length'], i['width'], i['height'], i['qty']]
    int_vals = [int(x) for x in vals]
    parsed_items.append(Item(i['name'], *int_vals))
prioritized_items = sorted(parsed_items, key = lambda x: -x.l * x.w * x.h)
from csv import DictWriter
with open('items.csv', 'w') as f:
    writer = DictWriter(f, prioritized_items[0]._asdict().keys())
    writer.writeheader()
    for i in prioritized_items:
        writer.writerow(i._asdict())
