# Pier2Pier.com/export
> The missing "export button" for pier2pier

Maybe you want to use the data you entered there somewhere else.
Maybe you want to reimport it after 6 months when it expires.*
Maybe you just want to prove you can.

\* I haven't check that this matches their reimport data structure, but presumably it's close.

# Prerequisites
- pipenv [Python 3 environment manager]
- unixy environment (not that hard to convert file path to Windows, though)
- pier2pier.com data (of course)!

# Usage
If you find you data trapped in their (thankfully, free) website, then:
- save the page html with all your item dimensions to this directory
- run `pipenv install` in this directory
- run `pipenv run python __init__.py`
and an [./items.csv] file with appear in this directory with all your item data.

# Contributing
I wrote this in 90 minutes and spent 30 minutes documenting and publishing it online.
So, if you actually find this useful and want to improve anything about it, please do!

